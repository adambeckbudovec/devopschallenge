#!/bin/bash
FILE0=~/Downloads/devops-code-challenge.tar
FILE1=~/Downloads/devops-code-challenge
UFW_STATUS1=active
UFW_STATUS0=inactive
NODE_STATUS=18.12.0
YARN_STATUS=1.22.19

#Ensure that you have GitLab account
echo "Make sure that you have a GitLab repo setup ahead of time"
sleep 3

if [ $(whoami) = 'root' ]; then
	echo "I Am Groot"
    sleep 3
fi
if [ -e $FILE0 ]
then
    echo "tar needs to be untarred"
    sudo tar -xvf $FILE0
    rm -rf $FILE0

else
    echo $FILE1
fi

#Install nodejs
echo "Installing nodejs"
sleep 2
sudo apt-get remove nodejs -y && sudo apt-get autoremove && sudo apt-get autoclean -y
sudo apt install npm -y
curl -sL https://deb.nodesource.com/setup_18.x -o nodesource_setup.sh
sudo bash nodesource_setup.sh
sudo apt install nodejs -y
node -v

#Install yarn
echo "Installing Yarn"
sleep 2
curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt install yarn -y
yarn --version
sudo chmod 777 -R ./

#Installing Kubectl from Package management
echo "Installing Kubectl from packages"
sleep 2
sudo apt-get update
sudo apt-get install -y ca-certificates curl
sudo apt-get install -y apt-transport-https
sudo curl -fsSLo /etc/apt/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
echo "deb [signed-by=/etc/apt/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
sudo apt-get install -y kubectl

#Install latest helm from scripts
echo "Installing helm"
sleep 2
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
sleep 10
rm -rf get_helm.sh
helm repo add gitlab https://charts.gitlab.io
helm repo update
helm upgrade --install adhd gitlab/gitlab-agent \
    --namespace gitlab-agent-adhd \
    --create-namespace \
    --set image.tag=v15.11.0-rc1 \
    --set config.token=QEC-kdiqVnobuyi-MuFhj-BnYnVz6xBnz7zzDBbPJt1bzmsv6Q \
    --set config.kasAddress=wss://kas.gitlab.com
#Installing GCP and remoting
echo "Getting GCP credenmtials"
sudo apt-get install apt-transport-https ca-certificates gnupg
echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo tee /usr/share/keyrings/cloud.google.gpg
sudo apt-get update && sudo apt-get install google-cloud-cli
sh gcpinit.sh&

#Getting TLS certification
echo "Installing certbots for TLS"
sudo snap install --classic certbot
sudo ln -s /snap/bin/certbot /usr/bin/certbot
sudo certbot certonly --standalone
sudo certbot renew --dry-run
sudo docker run -it --rm --name certbot \
            -v "/etc/letsencrypt:/etc/letsencrypt" \
            -v "/var/lib/letsencrypt:/var/lib/letsencrypt" \
            certbot/certbot certonly

#installing minikube
echo "Installing minikube"
sleep 2
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo install minikube-linux-amd64 /usr/local/bin/minikube
kubectl version --clien
sudo apt install docker -y
sudo apt isntall docker.io -y
sudo apt install docker-compose -y
sleep 2
minikube start
kubectl get po -A
minikube dashboard&
sleep 15
#Check to make sure that you are running either locally or through GCP
echo "Check to make sure that you are running either locally or through GCP"

sh dockerconfig.sh&