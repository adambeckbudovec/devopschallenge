#!/bin/bash
echo "Running commands for backend"
sleep 2
cd /home/ambrose157/Documents/GitLab/devopschallenge/
echo "Jumping to the backend"
cd backend/
sleep 1
# Install dependencies
echo yarn --frozen-lockfile --non-interactive
yarn --frozen-lockfile --non-interactive
# Build the app
echo yarn build
yarn build
#Test Run
echo "yarn test"
echo "Setting up Docker configuration"
sleep 1
yarn test
wait
yarn start&
cd /home/ambrose157/Documents/GitLab/devopschallenge/
