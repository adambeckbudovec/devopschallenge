#!/bin/bash
cd /home/ambrose157/Documents/GitLab/devopschallenge/
echo "Jumping to frontend"
cd frontend/
# Install dependencies
yarn --frozen-lockfile --non-interactive
# Build the image
# The following command will use environment variables defined in .env.production
echo yarn build --mode production
yarn build --mode production
sleep 3
#Test Run
echo yarn test
yarn test
wait
yarn serve dist&
sleep 5
echo "Finished testing"
cd /home/ambrose157/Documents/GitLab/devopschallenge/