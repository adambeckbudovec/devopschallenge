#!/bin/bash

echo "Jumping to backend"
sleep 1
cd frontend/
echo "Writing to Dockerfile"
sleep 2
printf "FROM node:18.12.0-alpine \n WORKDIR /home/ambrose157/Documents/GitLab/devopschallenge/ /app \n COPY . . \n COPY ./package.json /app \n COPY ./yarn.lock /app \n RUN yarn --frozen-lockfile \n RUN yarn build \n ENV PORT=8080 \n # Expose the port the applications will ba access from \n EXPOSE 8080\n CMD ["\"yarn"\", "\"start"\"]" > Dockerfile
echo "Finished writing to Dockerfile in frontend"
cd ..
cd backend/
sleep 1
echo "Writing to Dockerfile"
printf "FROM node:18.12.0-alpine \n WORKDIR /home/ambrose157/Documents/GitLab/devopschallenge/ /app \n COPY . . \n COPY ./package.json /app \n COPY ./yarn.lock /app \n RUN yarn --frozen-lockfile \n RUN yarn build \n ENV PORT=8080 \n # Expose the port the applications will ba access from \n EXPOSE 8080\n CMD ["\"yarn"\", "\"start"\"]" > Dockerfile
echo "Finished writing to Dockerfile in backend"
sleep 2
cd ..
sh backend.sh & 
sh frontend.sh &
wait
echo "backend and frontend should be able to run"

docker login registry.gitlab.com
docker build -t registry.gitlab.com/adambeckbudovec/devopschallenge backend/ && docker build -t registry.gitlab.com/adambeckbudovec/devopschallenge frontend/
docker push registry.gitlab.com/adambeckbudovec/devopschallenge
###echo "If error occurs remove containers and images"

<<com
cd backend/
docker build -t backend_test .
docker run -p 10000:10000 backend_test
###docker image rm node:18.12.0-alpine
###docker image rm backend_test:latest

###docker container rm docker container rm heuristic_wilbur
###docker container rm docker container rm zen_cerf 
com